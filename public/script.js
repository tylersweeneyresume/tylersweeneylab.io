const theme = document.getElementsByClassName("theme");
const words = document.getElementsByClassName("title-text");

const themes = ["#ededed", "#cc4444", "#4488cc", "#55bb55", "#cccc5c", "#bb66bb"]
let themePos = 0;

let wordArray = [];
let letterArray = [];
let animationProgress = 0;
let opac = 1;

let x, y, r, dx, dy, dr;

window.onload = (() => {
    if ('scrollRestoration' in history) {
        history.scrollRestoration = 'manual';
    }

    window.scrollTo(0, 0);

    for (let i = 0; i < words.length; i++) {
        const spans = words[i].querySelectorAll("span");

        for (let j = 0; j < spans.length; j++) {
            letterArray.push(new LetterSpecs(0, 0, 0));
        }

        wordArray.push(letterArray)
        letterArray = [];
    }
});

window.addEventListener("wheel", (e) => {
    console.log(animationProgress);
    const delta = Math.sign(e.deltaY);

    if (animationProgress === 6) {
        if (window.scrollY !== 0) return;

        if (Math.abs(delta) === delta) {
            document.body.style.overflow = "scroll";
        } else {
            document.body.style.overflow = "hidden";
        }
    }

    animationProgress += (Math.abs(delta) === delta) ? 1 : -1;
    animationProgress = Math.max(animationProgress, 0);
    animationProgress = Math.min(animationProgress, 6);

    for (let i = 0; i < words.length; i++) {
        const spans = words[i].querySelectorAll("span");
        for (let j = 0; j < spans.length; j++) {
            if (Math.abs(delta) === delta) {
                wordArray[i][j].x = Math.random() * 300 - 150;
                wordArray[i][j].y = Math.random() * 300 - 150;
                wordArray[i][j].r = Math.random() * 48 - 24;
                dx = wordArray[i][j].dx = 0;
                dy = wordArray[i][j].dy = 0;
                dr = wordArray[i][j].dr = 0;
            } else {
                if (wordArray[i][j].dx === 0) {
                    dx = wordArray[i][j].dx = wordArray[i][j].x * (1/(animationProgress+1));
                    dy = wordArray[i][j].dy = wordArray[i][j].y * (1/(animationProgress+1));
                    dr = wordArray[i][j].dr = wordArray[i][j].r * (1/(animationProgress+1));
                }

                x = wordArray[i][j].x = wordArray[i][j].x - wordArray[i][j].dx;
                y = wordArray[i][j].y = wordArray[i][j].y - wordArray[i][j].dy;
                r = wordArray[i][j].r = wordArray[i][j].r - wordArray[i][j].dr;
            }
            if (animationProgress !== 0) {
                spans[j].style.transform = `translate(${wordArray[i][j].x}%, ${wordArray[i][j].y}%) rotate(${wordArray[i][j].r}deg)`;
            } else {
                spans[j].style.transform = `translate(0%, 0%) rotate(0deg)`;
            }
        }
    }

    opac += (Math.abs(delta) !== delta) ? .2 : -.2;
    opac = Math.min(opac, 1);
    opac = Math.max(opac, 0);

    for (let i = 0; i < words.length; i++) {
        words[i].style.opacity = `${opac}`;
    }


});

window.addEventListener("click", () => {
    if (themePos < themes.length - 1) {
        themePos += 1;
    } else {
        themePos = 0;
    }

    for (let i = 0; i < theme.length; i++) {
        theme[i].style.color = themes[themePos];
        theme[i].style.borderColor = themes[themePos];
    }
})

const observer = new IntersectionObserver((entries) => {
    entries.forEach((entry) => {
        console.log(entry);
        if (entry.isIntersecting) {
            entry.target.classList.add('show');
        } else {
            entry.target.classList.remove('show');
        }
    })
})

const hiddenElements = document.querySelectorAll('.hidden');
hiddenElements.forEach((el) => observer.observe(el));

class LetterSpecs {
    x;
    y;
    r;
    dx;
    dy;
    dr;

    constructor(x, y, r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }
}